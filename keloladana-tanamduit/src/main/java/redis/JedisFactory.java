package redis;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisFactory {
	private static Logger logger = Logger.getLogger(JedisFactory.class);
	private static Integer globalPort;
	private static String host;
	private static Integer poolSize;
	private static Integer schemaUse;
	private static String jedisPswd;
	private static Integer connectionTimeout;
	
	
	public static JedisPool JedisConfig() {
		JedisPool jedisPool = null;
		JedisPoolConfig poolConfig = new JedisPoolConfig();
		
		
		poolConfig.setMaxTotal(poolSize);
		
		// get database schema used
		int schemaUsed = schemaUse;
		
		System.out.println("Schema Used: " + schemaUsed);
		String jedisPwd = jedisPswd;
		
		if (jedisPwd.trim().length() > 0) {
			System.out.println("Initiating Redis Using Password");
			jedisPool = new JedisPool(poolConfig,host,globalPort,connectionTimeout,jedisPswd, schemaUsed);
			

		} else {
			System.out.println("Initiating Redis Without Password");
			jedisPool = new JedisPool(poolConfig,host,globalPort,connectionTimeout,null, schemaUsed);
		}
		return jedisPool;
	}

	public static String decrypt(String encrypted) {
		String decoded = "";

		try {
			byte[] password = encrypted.getBytes();
			decoded = new String(Base64.decodeBase64(password));
		} catch (Exception e) {

		}

		return decoded;
	}

	public static String get(String key) throws Exception {
		String result = "";
		JedisPool jedis;
		jedis = JedisConfig();
		
		try {

			try {
				/*byte[] t = key.getBytes();
				key = new String(Base64.encodeBase64(t));*/
				result = jedis.getResource().get(key);
			} catch (Exception e) {
				logger.info("Failed Safe Encoder : " + e);
			}
		} catch (Exception e) {
			logger.info(e);
			logger.info("Jedis is null");
		}

		finally {
			jedis.destroy();
		}

		String res = null;
		if (result != null) {
			res = result.toString();
		}
		return res;
	}
	
	
	public static Long incr(String key) throws Exception {
		Long result = null;
		String value="";
		JedisPool jedis;
		jedis = JedisConfig();
		
		try {

			try {
				/*byte[] t = key.getBytes();
				key = new String(Base64.encodeBase64(t));*/
				result = jedis.getResource().incr(jedis.getResource().get(key));
				value = jedis.getResource().get(key);
				System.out.println("key :"+key);
				System.out.println("result :"+result);
				System.out.println("value :"+value);
			} catch (Exception e) {
				logger.info("Failed Safe Encoder : " + e);
			}
		} catch (Exception e) {
			logger.info(e);
			logger.info("Jedis is null");
		}

		finally {
			jedis.destroy();
		}

		String val = null;
		if (value != null) {
			val = value;
		}
		return result;
	}
	
	

	public static String setex(String key, int TTL, String value) throws Exception {
		String result = null;
		String key1 = "";
		String value1 = "";
		JedisPool jedis = JedisConfig();

		try {
			byte[] keyAsBytes = key.getBytes();
			key1 = new String(Base64.encodeBase64(keyAsBytes));
			byte[] valueAsBytes = value.getBytes();
			value1 = new String(Base64.encodeBase64(valueAsBytes));
			result = jedis.getResource().setex(key1, TTL, value1);
		} catch (Exception e) {
			logger.info("Failed Safe Encoder : " + e);
		}

		jedis.destroy();

		return result;
	}

	public static String setIncr(String key, String value) throws Exception {
		String result = "";
		JedisPool jedis = JedisConfig();

		/*byte[] keyAsBytes = key.getBytes();
		String key1 = new String(Base64.encodeBase64(keyAsBytes));
		byte[] valueAsBytes = value.getBytes();
		String value1 = new String(Base64.encodeBase64(valueAsBytes));*/
		result = jedis.getResource().set(key, value);

		jedis.destroy();
		return result;
	}
	
	public static String set(String key, String value) throws Exception {
		String result = "";
		JedisPool jedis = JedisConfig();
		Long messageId;
		Map<String, String> hash = new HashMap<String, String>();
		
		messageId = jedis.getResource().incr("all:message");
		hash.put("id", messageId+"");
		hash.put("message_data", new Random()+"");
		
		jedis.getResource().hmset(Long.toString(messageId), hash);
		jedis.getResource().lpush(key.toString(), value);
		
		jedis.destroy();
		return result;
	}

	public static String hset(String key, String field, String value) throws Exception {
		long result = (long) -1;
		JedisPool jedis = JedisConfig();

		byte[] keyAsBytes = key.getBytes();
		String key1 = new String(Base64.encodeBase64(keyAsBytes));
		byte[] fieldAsBytes = field.getBytes();
		String field1 = new String(Base64.encodeBase64(fieldAsBytes));
		byte[] valueAsBytes = value.getBytes();
		String value1 = new String(Base64.encodeBase64(valueAsBytes));
		result = jedis.getResource().hset(key1, field1, value1);
		String res = Long.toString(result);

		jedis.destroy();
		return res;
	}

	public static String hget(String key, String field) throws Exception {
		String result = "";
		JedisPool jedis = JedisConfig();

		byte[] keyAsBytes = key.getBytes();
		String key1 = new String(Base64.encodeBase64(keyAsBytes));
		byte[] fieldAsbytes = field.getBytes();
		String field1 = new String(Base64.encodeBase64(fieldAsbytes));

		result = jedis.getResource().hget(key1, field1);

		jedis.destroy();
		return result;
	}

	public static String hdel(String key, String field) throws Exception {
		Long result = (long) -1;
		String status = "";
		JedisPool jedis = JedisConfig();
		try {
			byte[] keyAsBytes = key.getBytes();
			String key1 = new String(Base64.encodeBase64(keyAsBytes));
			byte[] fieldAsbytes = field.getBytes();
			String field1 = new String(Base64.encodeBase64(fieldAsbytes));

			result = jedis.getResource().hdel(key1, field1);

			status = "SUCCESS";
		} catch (Exception e) {
			status = "FAILED";
			logger.info("Failed Safe Encoder : " + e);
		}

		jedis.destroy();
		return status;
	}

	public static String delete(String key) throws Exception {
		Long result = (long) -1;
		String status = "";
		JedisPool jedis = JedisConfig();

		try {
			/*byte[] keyAsBytes = key.getBytes();
			keyAsBytes = Base64.encodeBase64(keyAsBytes);*/

			result = jedis.getResource().del(key);

			status = "SUCCESS";
		} catch (Exception e) {
			status = "FAILED";
			logger.info("Failed Safe Encoder : " + e);
		}
		jedis.destroy();

		return status;
	}

	public static String expire(String key, int timeout) throws Exception {
		Long set_timeout = (long) 0;
		JedisPool jedis = JedisConfig();

		try {
			byte[] keyAsBytes = key.getBytes();
			String key1 = new String(Base64.encodeBase64(keyAsBytes));
			set_timeout = jedis.getResource().expire(key1, timeout);
		} catch (Exception e) {
			logger.info("Failed Safe Encoder : " + e);
		}

		Long result = set_timeout;
		return result.toString();
	}

	public static String ping() throws Exception {
		String result = null;
		JedisPool jedis = JedisConfig();

		try {
			result = jedis.getResource().ping();

		} catch (Exception e) {
			logger.info("Failed to Ping !" + e);
		}

		return result;
	}
	
	
	public void setGlobalPort(Integer globalPort){
		this.globalPort = globalPort;
	}
	
	public void setHost(String host){
		this.host = host;
	}
	
	public void setPoolSize(Integer poolSize){
		this.poolSize = poolSize;
	}
	
	public void setSchemaUse(Integer schemaUse){
		this.schemaUse = schemaUse;
	}
	
	public void setJedisPswd(String jedisPswd){
		this.jedisPswd = jedisPswd;
	}
	
	public void setConnectionTimeout(Integer connectionTimeout){
		this.connectionTimeout = connectionTimeout;
	}
}
