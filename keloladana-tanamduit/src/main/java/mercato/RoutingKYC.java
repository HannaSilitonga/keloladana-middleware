package mercato;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

public class RoutingKYC implements Callable{

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		// TODO Auto-generated method stub
		Object payload = eventContext.getMessage().getPayload();
		String filePath = payload.toString();
		
		File file = new File(filePath);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		
		String header = reader.readLine();
		//header is the first line.
		
		String[] strArray = header.split("\\|");
		int countHeader = strArray.length;
		System.out.println("COUNT OF HEADER KYC FILE : "+countHeader);
		return countHeader;
	}

}
