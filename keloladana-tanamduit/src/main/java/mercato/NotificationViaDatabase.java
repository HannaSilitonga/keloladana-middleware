package mercato;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

public class NotificationViaDatabase {
	private JdbcTemplate jdbcTemplate;
	private Logger logger;

	public NotificationViaDatabase() {
		logger = Logger.getLogger(NotificationViaDatabase.class);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public void setGetDataSource(DataSource getDataSource) {
		this.setJdbcTemplate(new JdbcTemplate(getDataSource));
	}

	@SuppressWarnings("unused")
	public Object onCall(HashMap<String, String> request) throws ParseException {

		Object result = "failed";
		String sql = "";

		String user_id, method, status, filename, idNotif, fileNameFailed = null;
		String _row, _rowOfSubs = null, _rowOfRedeem, _rowOfSwitching = null, _rowOfKYCCorp, _rowOfKYCIndv, _rowKYCIndvFailed , _rowCountBank, _fileNameFailed;
		Integer row, file, rowOfSubs = null, rowOfRedeem, rowOfSwitching, rowOfKYCCorp, rowOfKYCIndv, rowOfKYCIndvFailed = null, fileOfSubs, fileOfRedeem, fileOfSwitching,
				fileOfKYCCorp, fileOfKYCIndv = null, rowCountBank, fileCountBank;

		System.out.println("ROW BEFORE : " + request.get("row"));

		user_id = request.get("user_id");
		method = request.get("method");
		status = request.get("status");
		_row = request.get("row");
		_rowOfRedeem = request.get("rowOfRedeem");
		_rowOfSubs = request.get("rowOfSubs");
		_rowOfKYCCorp = request.get("rowKYCCorporate");
		_rowOfKYCIndv = request.get("rowKYCIndividual");
		_rowKYCIndvFailed = request.get("rowKYCIndividualFailed");
		_rowCountBank = request.get("rowCountBank");
		_fileNameFailed = request.get("fileNameFailed");
		filename = request.get("fileName");
		idNotif = request.get("idNotif");
		

		System.out.println("ID NOTIF : " + filename);

		if (_row != null) {
			row = Integer.parseInt(_row);
			Double _file = Math.ceil((float) row / 100);
			file = _file.intValue();
		} else {
			row=0;
			file = 0;
		}

		if (_rowOfSubs != null) {
			rowOfSubs = Integer.parseInt(_rowOfSubs);
			Double _fileOfSubs = Math.ceil((float) rowOfSubs / 100);
			fileOfSubs = _fileOfSubs.intValue();
		} else {
			rowOfSubs = 0;
			fileOfSubs = 0;
		}

		if (_rowOfRedeem != null) {
			rowOfRedeem = Integer.parseInt(_rowOfRedeem);
			Double _fileOfRedeem = Math.ceil((float) rowOfRedeem / 100);
			fileOfRedeem = _fileOfRedeem.intValue();
		} else {
			rowOfRedeem = 0;
			fileOfRedeem = 0;
		}
		
		if (_rowOfSwitching != null) {
			rowOfSwitching = Integer.parseInt(_rowOfSwitching);
			Double _fileOfSwitching = Math.ceil((float) rowOfSwitching / 100);
			fileOfSwitching = _fileOfSwitching.intValue();
		} else {
			rowOfSwitching = 0;
			fileOfSwitching = 0;
		}

		if (_rowOfKYCCorp != null) {
			rowOfKYCCorp = Integer.parseInt(_rowOfKYCCorp);
			Double _fileOfKYCCorp = Math.ceil((float) rowOfKYCCorp / 100);
			fileOfKYCCorp = _fileOfKYCCorp.intValue();
		} else {
			rowOfKYCCorp = 0;
			fileOfKYCCorp = 0;
		}

		if (_rowOfKYCIndv != null) {
			rowOfKYCIndv = Integer.parseInt(_rowOfKYCIndv);
			Double _fileOfKYCIndv = Math.ceil((float) rowOfKYCIndv / 100);
			fileOfKYCIndv = _fileOfKYCIndv.intValue();
		} else {
			rowOfKYCIndv = 0;
			fileOfKYCIndv = 0;
		}
		if (_rowKYCIndvFailed != null) {
			rowOfKYCIndvFailed = Integer.parseInt(_rowKYCIndvFailed);
		}
		
		if (_rowCountBank != null) {
			rowCountBank = Integer.parseInt(_rowCountBank);
			Double _fileCountBank = Math.ceil((float) rowCountBank / 100);
			fileCountBank = _fileCountBank.intValue();
		} else {
			rowCountBank = 0;
			fileCountBank = 0;
		}
		
		if (_fileNameFailed == null) {
			fileNameFailed = "-";
		}else{
			fileNameFailed = _fileNameFailed;
		}

		try {

			if (idNotif == null || idNotif == "") {
				sql = "INSERT INTO smc_admin_notification(user_id, method, status, notif_status, row_of_subs, file_of_subs, row_of_redeem, file_of_redeem, row_of_switching, file_of_switching, row_kyc_corp, file_kyc_corp, row_kyc_indv, row_kyc_indv_failed, file_kyc_indv, row_ofBankAcc, file_ofBankAcc, created_date, file_name, file_name_failed) VALUES (" + 
				          user_id + ",'" + method + "','" + status + "'," + 0 + "," + 
				          rowOfSubs + "," + fileOfSubs + "," + rowOfRedeem + "," + 
				          fileOfRedeem + "," + rowOfSwitching + "," + fileOfSwitching + "," + rowOfKYCCorp + "," + fileOfKYCCorp + "," + 
				          rowOfKYCIndv + "," + rowOfKYCIndvFailed + "," + fileOfKYCIndv + "," + rowCountBank + "," + fileCountBank + ",'" + createdDateTime() + "','" + 
				          filename + "','" + fileNameFailed + "')";
			} else {
				sql = "INSERT INTO smc_admin_notification(id, user_id, method, status, notif_status, row_of_subs, file_of_subs, row_of_redeem, file_of_redeem, row_of_switching, file_of_switching, row_kyc_corp, file_kyc_corp, row_kyc_indv, row_kyc_indv_failed, file_kyc_indv, row_ofBankAcc, file_ofBankAcc, created_date, file_name, file_name_failed) VALUES (" + 
				          idNotif + "," + user_id + ",'" + method + "','" + status + "'," + 0 + "," + 
				          rowOfSubs + "," + fileOfSubs + "," + rowOfRedeem + "," + 
				          fileOfRedeem + "," + rowOfSwitching + "," + fileOfSwitching + "," + rowOfKYCCorp + "," + fileOfKYCCorp + "," + 
				          rowOfKYCIndv + "," + rowOfKYCIndvFailed + "," + fileOfKYCIndv + "," + rowCountBank + "," + fileCountBank + ",'" + createdDateTime() + "','" + 
				          filename + "','" + fileNameFailed + "')" + "ON DUPLICATE KEY UPDATE status = " + "'" + status + "' , row_of_subs = '" + rowOfSubs + "' , file_of_subs = '" + fileOfSubs + "'" + 
				          ", row_of_redeem = '" + rowOfRedeem + "' , file_of_redeem = '" + fileOfRedeem + "'" + 
				          ", row_of_switching = '" + rowOfSwitching + "' , file_of_switching = '" + fileOfSwitching + "'" + 
				          ", row_kyc_corp = '" + rowOfKYCCorp + 
				          "' , file_kyc_corp = '" + fileOfKYCCorp + "'" + ", row_kyc_indv = '" + rowOfKYCIndv + "' , file_kyc_indv = '" + 
				          fileOfKYCIndv + "', " + "row_ofBankAcc = '" + rowCountBank + "' , file_ofBankAcc = '" + fileCountBank + "'";
			}

			this.jdbcTemplate.execute(sql);
			System.out.println("SQL " + sql);
			result = "success";
		} catch (Exception e) {
			System.out.println("ERRROR " + e);
			result = "failed";
			return e;
		}

		return result;
	}

	public String createdDateTime() {
		String dateTime = "";

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		dateTime = dateFormat.format(date);

		return dateTime;
	}

}
