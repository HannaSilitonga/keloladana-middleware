package mercato;

import org.apache.commons.lang.time.FastDateFormat;
import org.mule.DefaultMuleMessage;
import org.mule.MessageExchangePattern;
import org.mule.api.FutureMessageResult;
import org.mule.api.MuleContext;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.api.MuleSession;
import org.mule.api.client.MuleClient;
import org.mule.api.construct.FlowConstruct;
import org.mule.api.endpoint.EndpointURI;
import org.mule.api.endpoint.InboundEndpoint;
import org.mule.api.endpoint.OutboundEndpoint;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transaction.Transaction;
import org.mule.api.transaction.TransactionException;
import org.mule.api.transformer.DataType;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractTransformer;

import java.util.Map;
import java.util.Properties;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

public class CustomTransformer  extends AbstractTransformer{

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected Object doTransform(Object src, String enc)
			throws TransformerException {
		
		final StringBuilder strRowBuilder = new StringBuilder();
		String result = "";
		try {
			final LinkedList<Map> records = (LinkedList<Map>)src;

			if (records.size() > 0) {
				// File Header
				 strRowBuilder.append(this.header);

				for (int i=0; i<records.size(); i++) {
					strRowBuilder.append(this.constructRowData(records.get(i)));
				}
				result = strRowBuilder.toString();
			} 
			
		} catch (final Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}
		
		return result.getBytes();	
	
	}
	
	private String header = "";
	private String newLine = "\n";
	protected String separator = "|";
	protected ArrayList fields;
	
	public void setSeparator(String _separator) {
		this.separator = _separator.trim();
	}

	public void setFields(final ArrayList _obj) {
		this.fields = _obj;
		this.contructHeader();
	}	
	
	private void contructHeader() {
		final StringBuilder strHeader = new StringBuilder("");

		for (int j = 1; j <= this.fields.size(); j++) {
			final Properties p = (Properties) this.fields.get(j-1);
			strHeader.append(p.getProperty("fieldname"));
			if (j < this.fields.size()) {
				strHeader.append(this.separator);
			} else {
				strHeader.append(this.newLine);
			}
		
		}
		
		this.header = strHeader.toString();
	}
	
	private String constructRowData(final Map<String, Object> map) {
		final StringBuilder strRowBuilder = new StringBuilder("");
		
		for (int j = 1; j <= this.fields.size(); j++) {
			strRowBuilder.append(this.getMapData(map, j));

			if (j < this.fields.size()) {
				strRowBuilder.append(this.separator);
			} else {
				strRowBuilder.append(this.newLine);
			}
		}
		
		return strRowBuilder.toString();
	}
	
	private String getMapData(final Map<String, Object> map, int pos) {
		final Properties p = (Properties) this.fields.get(pos-1);
		final StringBuilder strMapDataBuilder = new StringBuilder("");

		try {
			if (map.get(p.getProperty("fieldname")) != null ) {
				switch (p.getProperty("fieldtype")) {
					case "DATE":
						final FastDateFormat df1 = FastDateFormat.getInstance(p.getProperty("fieldformat"));
						strMapDataBuilder.append(df1.format(map.get(p.getProperty("fieldname"))));
						break;
					case "DATETIME":
						final FastDateFormat df2 = FastDateFormat.getInstance(p.getProperty("fieldformat"));
						strMapDataBuilder.append(df2.format(map.get(p.getProperty("fieldname"))));
						break;
					case "NUMBER":
						strMapDataBuilder.append(String.format(p.getProperty("fieldformat"), map.get(p.getProperty("fieldname"))));
						break;
					case "STRING":
					default:
						strMapDataBuilder.append(map.get(p.getProperty("fieldname")));	
						break;
				}
			} else {
				strMapDataBuilder.append(" ");
			}
		} catch (final Exception e) {
			
		}

		return strMapDataBuilder.toString();
	}
}
