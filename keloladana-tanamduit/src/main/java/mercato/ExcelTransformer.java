package mercato;

import org.mule.api.MuleEventContext;
import org.mule.api.client.MuleClient;
import org.mule.api.lifecycle.Callable;
import java.nio.file.Files;

import java.util.Map;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelTransformer implements Callable {

	@SuppressWarnings("unchecked")
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		Object result = "FAILED";

		try {
			MuleClient muleClient = eventContext.getMuleContext().getClient();
			Object payload = eventContext.getMessage().getPayload();

			HashMap<String, String> allData = (HashMap<String, String>) payload;

			// header
			Object objHeader = allData.get("header");
			HashMap<String, String> header = (HashMap<String, String>) objHeader;

			// path
			String path = allData.get("path");

			// template path
			String pathTemplate = allData.get("pathTemplate");

			// details data utama
			Object data = allData.get("data");
			LinkedList<Map> detailsData = (LinkedList<Map>) data;

			// source folder that save template File
			File source = new File(pathTemplate);

			File dest = new File(path);
			if (!dest.exists()) {
				dest.mkdir();
			}
			
			//copy template file to main directory that visible for backoffice mercato in middle office application
			copyFileUsingApache(source, dest);
			updateExcel(path, detailsData, header);
			
			result = "SUCCESS";
			
		} catch (Exception e) {
			System.out.println("EXCEL TRANSFORMER EXCEPTION : " + e.toString());
			result = "FAILED";
		}
		return result;
	}

	public static void copyFileUsingApache(File from, File to) throws IOException {
		FileUtils.copyFile(from, to);
	}

	//UPDATE EXISTING EXCEL (UPDATE TEMPLATE THAT IS PROVIDED BEFORE)
	private void updateExcel(String dest, LinkedList<Map> detailsData, Map header)
			throws IOException, InvalidFormatException {

		FileInputStream inputStream = new FileInputStream(new File(dest));
		Workbook workbook = WorkbookFactory.create(inputStream);

		Sheet sheet = workbook.getSheetAt(0);

		Object[] headerArr = new Object[header.size()];

		for (int i = 0; i < header.size(); i++) {
			headerArr[i] = header.get(Integer.toString(i));
		}

		Map<String, Object[]> empinfo = new TreeMap<String, Object[]>();

		int idxMain = 1;
		for (int i = 0; i < detailsData.size(); i++) {
			Object[] allData = new Object[detailsData.get(i).size()];
			for (int j = 0; j < detailsData.get(i).size(); j++) {
				allData[j] = detailsData.get(i).get(Integer.toString(j));
			}
			empinfo.put(idxMain + "", allData);
			idxMain++;
		}

		// Iterate over data and write to sheet
		int rowCount = 0;

		for (int i = 1; i <= empinfo.size(); i++) {
			String b = String.valueOf(i);

			Row row = sheet.createRow(++rowCount);
			Object[] objectArr = empinfo.get(b);

			int columnCount = 0;

			Cell cell = row.createCell(columnCount);
			cell.setCellValue(rowCount);

			for (Object field : objectArr) {
				cell = row.createCell(columnCount++);
				if (field instanceof String) {
					cell.setCellValue((String) field);
				} else if (field instanceof Integer) {
					cell.setCellValue((Integer) field);
				} else if (field instanceof Long) {
					cell.setCellValue((Long) field);
				}
			}
		}

		inputStream.close();

		FileOutputStream outputStream = new FileOutputStream(dest);
		workbook.write(outputStream);
		outputStream.close();

	}

}
