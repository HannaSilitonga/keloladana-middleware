package mercato;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.python.antlr.PythonParser.print_stmt_return;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class LoadDataInfile {
	
	@Value("${bulk.kyc.table.name}")
    String bulk_kyc_tblName;
	
	@Value("${bulk.kyc.table.column}")
    String bulk_kyc_tblColoumn;
	
	@Value("${bulk.kyc.column.name}")
    String bulk_kyc_col_name;
	
	private JdbcTemplate jdbcTemplate;
	private Logger logger;

	public LoadDataInfile() {
		logger = Logger.getLogger(LoadDataInfile.class);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public void setGetDataSource(DataSource getDataSource) {
		this.setJdbcTemplate(new JdbcTemplate(getDataSource));
	}

	public Object onCall(HashMap<String, String> request) throws ParseException {
		Object result = "failed";
		Map<String, Object> data;
		String filePath = (request.get("filePath")).replace("\\", "\\\\");
		String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
		String tableName = request.get("tableName");
		String sql = "";
		String sql2="";

		doIt(filePath, tableName);

		try {
		if (tableName.equals(bulk_kyc_tblName)) {
			sql = "LOAD DATA LOCAL INFILE '" + filePath + "' " + "IGNORE INTO TABLE " + tableName + " "
					+ "FIELDS TERMINATED BY '|' " + "LINES TERMINATED BY '\n' " + " IGNORE 1 LINES "
					+ "("+bulk_kyc_tblColoumn+")";
			
			this.getJdbcTemplate().execute(sql);
			
			sql2 = "UPDATE " +tableName+" SET "+bulk_kyc_col_name+"='"+fileName+"' WHERE "+bulk_kyc_col_name+" IS NULL";
			
			this.getJdbcTemplate().execute(sql2);
		}else{
		sql = "LOAD DATA LOCAL INFILE '" + filePath + "' " + "IGNORE INTO TABLE " + tableName + " "
				+ "FIELDS TERMINATED BY '|' " + "LINES TERMINATED BY '\n' " + " IGNORE 1 LINES ";
			this.getJdbcTemplate().execute(sql);
		}
		
		result = "success";
		
		} catch (Exception e) {
			result="failed";
			return e;
		}
		System.out.println("RESULT SQL LOAD DATA INFILE : " + sql);
		
		return result;
	}

	public void doIt(String filePath, String tableName) throws ParseException {
		StringBuilder sb = new StringBuilder();
		try {
			File file = new File(filePath);
			BufferedReader reader = new BufferedReader(new FileReader(file));
			int row = 0;
			String line = "", oldtext = "", newtext = "";
			while ((line = reader.readLine()) != null) {
				line = line + "\r\n";
				String[] split = line.split("\\|");
				for (int i = 0; i < split.length; i++) {
					if (i != split.length - 1) {
						if (split[i].equals("") || split[i].equals(" ")) {
							split[i] = "\\N";

						}
						split[i] = split[i] + "|";
					}
					if (split[i].equals("\r\n") || split[i].equals(" \r\n")) {
						if (tableName.contains("ifua")) {
							split[i] = "0000-00-00" + "\r\n";
						} else {
							split[i] = "\\N" + "\r\n";
						}
					}
					if (row != 0) {
						if (tableName.contains("reconcile")) {
							if (i == 0) {
								Date tradeDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(split[i]);
								String krwtrDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(tradeDate);
								split[i] = krwtrDate + "|";
							} else if (i == 20) {
								Date tradeDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(split[i]);
								String krwtrDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(tradeDate);
								split[i] = krwtrDate + "\r\n";
							}
						}
					}
					sb.append(split[i]);
				}
				row++;
			}
			reader.close();

			FileWriter writer = new FileWriter(filePath);
			writer.write(sb.toString());

			writer.close();

		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

}
