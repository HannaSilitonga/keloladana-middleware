package mercato;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class deleteFile implements Callable {

	List<String> filesListInDir = new ArrayList<String>();

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		String zipperStatus = "";
		Object payload = eventContext.getMessage().getPayload();
		
		@SuppressWarnings("unchecked")
		HashMap<String, String> path = (HashMap<String, String>)payload;
		
		
		return zipperStatus;
	}

	
	private void deleteFolder(File dir){
		String[]entries = dir.list();
		for(String s: entries){
		    File currentFile = new File(dir.getPath(),s);
		    currentFile.delete();
		}
		dir.delete();
	}
}
