package mercato;

import org.apache.commons.codec.binary.Base64;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class sendEmail implements Callable {

	List<String> filesListInDir = new ArrayList<String>();
	@Value("${email.subject}")
    String subject;
	
	@Value("${email.username}")
    String username_;
	
	@Value("${email.password}")
    String password_;
	
	@Value("${email.server.port}")
    String port;
	
	@Value("${email.server.host}")
    String host;
	
	@Value("${email.smtp.starttls.enable}")
    String enableTLS;
	
	@Value("${url.activation}")
    String urlActivation;
	
	
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		final String username = username_;
		final String password = password_;
		String body = "";
		String subject = "";
		
		Object payload = eventContext.getMessage().getPayload();
		
		
		@SuppressWarnings("unchecked")
		HashMap<String, String> detailsEmail = (HashMap<String, String>)payload;
		subject=detailsEmail.get("subject");
		body=detailsEmail.get("message");
		
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", enableTLS);
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);

		
		System.out.println("USERNAME : "+username+" PORT : "+port+ "TLS : "+props.getProperty("mail.smtp.starttls.enable"));
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setContent(body, "text/html; charset=utf-8");
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(detailsEmail.get("email")));
			message.setSubject(subject);

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		
		return "success";
	}

	/**
	 * This method zips the directory
	 * 
	 * @param dir
	 * @param zipDirName
	 */

}
