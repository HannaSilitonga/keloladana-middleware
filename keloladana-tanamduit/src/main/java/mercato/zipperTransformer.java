package mercato;

import org.apache.commons.io.FileUtils;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class zipperTransformer implements Callable {
	
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		Object payload = eventContext.getMessage().getPayload();
		@SuppressWarnings("unchecked")
		HashMap<String, String> path = (HashMap<String, String>) payload;
		HashMap<String, String> zipperStatus = new HashMap<String, String>();
		
		try
        {
                String zipFile = path.get("dirTemp")+".zip";
                String sourceDirectory = path.get("dirTemp");
                
                String[] zipName_ = path.get("dirTemp").split("/");
                String zipName = zipName_[(zipName_.length)-1];
               
                //create byte buffer
                byte[] buffer = new byte[1024];
                /*
                 * To create a zip file, use
                 *
                 * ZipOutputStream(OutputStream out)
                 * constructor of ZipOutputStream class.
                 *  
                 */
                 
                 //create object of FileOutputStream
                 FileOutputStream fout = new FileOutputStream(zipFile);
                 
                 //create object of ZipOutputStream from FileOutputStream
                 ZipOutputStream zout = new ZipOutputStream(fout);
                 
                 //create File object from directory name
                 File dir = new File(sourceDirectory);
                 
                 //check to see if this directory exists
                 if(!dir.isDirectory())
                 {
                        System.out.println(sourceDirectory + " is not a directory");
                 }
                 else
                 {
                        File[] files = dir.listFiles();
                       
                        for(int i=0; i < files.length ; i++)
                        {
                                System.out.println("Adding " + files[i].getName());
                               
                                //create object of FileInputStream for source file
                                FileInputStream fin = new FileInputStream(files[i]);
                 
                                /*
                                 * To begin writing ZipEntry in the zip file, use
                                 *
                                 * void putNextEntry(ZipEntry entry)
                                 * method of ZipOutputStream class.
                                 *
                                 * This method begins writing a new Zip entry to
                                 * the zip file and positions the stream to the start
                                 * of the entry data.
                                 */
                 
                                zout.putNextEntry(new ZipEntry(files[i].getName()));
                 
                                /*
                                 * After creating entry in the zip file, actually
                                 * write the file.
                                 */
                                int length;
                 
                                while((length = fin.read(buffer)) > 0)
                                {
                                   zout.write(buffer, 0, length);
                                }
                 
                                /*
                                 * After writing the file to ZipOutputStream, use
                                 *
                                 * void closeEntry() method of ZipOutputStream class to
                                 * close the current entry and position the stream to
                                 * write the next entry.
                                 */
                 
                                 zout.closeEntry();
                                 
                                 //close the InputStream
                                 fin.close();
                        }
                 }
                 
                
                  //close the ZipOutputStream
                  zout.close();
                 
                  //Move zip file to directory "to_sinvest" 
                  Path movefrom = FileSystems.getDefault().getPath(zipFile);
                  Path target = FileSystems.getDefault().getPath(path.get("dir")+"/"+zipName+".zip");
                  Files.move(movefrom, target, StandardCopyOption.REPLACE_EXISTING);
                  
                  //Delete File
                  /*Files.delete(FileSystems.getDefault().getPath(path.get("dirTemp")));
                  */
                  
                  zipperStatus.put("status", "success");
                  zipperStatus.put("rowCount", path.get("rowCount"));
                  System.out.println("Zip file has been created!");
       
        }
        catch(IOException ioe)
        {	
        		zipperStatus.put("status", "failed");
                System.out.println("IOException :" + ioe);
        }

		return zipperStatus;
	}
	
	
}
