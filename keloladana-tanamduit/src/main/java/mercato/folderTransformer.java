package mercato;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.mule.api.MuleEventContext;
import org.mule.api.client.MuleClient;
import org.mule.api.lifecycle.Callable;
import org.springframework.beans.factory.annotation.Value;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class folderTransformer implements Callable {

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		MuleClient muleClient = eventContext.getMuleContext().getClient();
		Object payload = eventContext.getMessage().getPayload();

		File file = new File(payload.toString());
		boolean b = false;

		if (!file.exists()) {
			b = file.mkdirs();
		}
		if (b)
			System.out.println("Directory successfully created");
		else
			System.out.println("Failed to create directory");

		return null;
	}

}
