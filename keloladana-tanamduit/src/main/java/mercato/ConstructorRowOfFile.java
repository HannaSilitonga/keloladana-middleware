package mercato;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.client.MuleClient;
import org.mule.api.lifecycle.Callable;

public class ConstructorRowOfFile implements Callable {

	@SuppressWarnings("unchecked")
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {

		MuleClient muleClient = eventContext.getMuleContext().getClient();
		Object payload = eventContext.getMessage().getPayload();
		
		HashMap<String, String> detailsData = (HashMap<String, String>)payload;
		
		Object data = detailsData.get("data");
		
		final LinkedList<Map> records = (LinkedList<Map>)data;
		LinkedList<Map> recordsToBeProcess = new LinkedList<Map>();
		int i = 0; //as totalRow
		int j = 0; //as totalFile
		HashMap<String, String> result = new HashMap<>();

		
		if (records.size() > 0) {
			for (i = 0; i < records.size(); i++) {
				recordsToBeProcess.add(records.get(i));
				if ((i % 99 == 0 && i != 0) || i == records.size() - 1) {
					MuleMessage message = new DefaultMuleMessage(recordsToBeProcess, eventContext.getMuleContext());
					recordsToBeProcess = new LinkedList<Map>(); 
					System.out.println("CREATE FILE");
					muleClient.send("vm://"+detailsData.get("vm"), message);
					j++;
				}
			}
		}
		result.put("executedRow", i+"");
		result.put("executedFile", j+"");
		
		return result;
	}
}
