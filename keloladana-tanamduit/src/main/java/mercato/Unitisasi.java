package mercato;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.python.antlr.PythonParser.print_stmt_return;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class Unitisasi {
	private JdbcTemplate jdbcTemplate;
	private Logger logger;

	public Unitisasi() {
		logger = Logger.getLogger(Unitisasi.class);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public void setGetDataSource(DataSource getDataSource) {
		this.setJdbcTemplate(new JdbcTemplate(getDataSource));
	}

	public Object onCall(HashMap<String, String> request) throws ParseException {

		Object result = null;
		Map<String, Object> data;
		String nav_date = (request.get("nav_date"));
		Object hasil = "FAILED";
		LocalDate navDate = null;
		JSONObject navProduct = new JSONObject();
		
		LocalDate currentDate = LocalDate.now() ;
		navDate = LocalDate.parse(nav_date);
		
		Long days_ = ChronoUnit.DAYS.between(navDate, currentDate );
		int days = days_.intValue();
		
		//System.out.println("DAYS : "+days);
		//System.out.println("NAV DATE : "+navDate);
		//System.out.println("CURRENT DATE : "+currentDate);
		
		//for(int x=0;x<=days - 1;x++){
			//String dateUnit = "\"" + navDate.plusDays(new Long(x))+ "\"";
			hasil = this.jdbcTemplate.queryForObject("CALL sp_unitisasi('"+nav_date+"')", new ResultRowMapper());
			System.out.println("--------------------- DATE UNITISASI ---------------------- "+nav_date);
			System.out.println("--------------------- HASIL ---------------------- "+hasil);
			
			if(hasil.equals("success")){
				hasil = "success";
			}
			try {
				navProduct.put(navDate+"", hasil);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//}

		return hasil;
	}
	
	public class ResultRowMapper implements RowMapper {

		@Override
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			Object hasil = null;
			hasil = rs.getObject(1);
			return hasil;
		}
	}

}
