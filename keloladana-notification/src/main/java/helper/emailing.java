package helper;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;
import org.springframework.beans.factory.annotation.Value;

public class emailing
  implements Callable
{
  List<String> filesListInDir = new ArrayList();

  @Value("${email.username}")
  String username_;

  @Value("${email.password}")
  String password_;

  @Value("${email.server.port}")
  String port;

  @Value("${email.server.host}")
  String host;

  @Value("${email.smtp.starttls.enable}")
  String enableTLS;

  public Object onCall(MuleEventContext eventContext) throws Exception { final String username = this.username_;
    final String password = this.password_;
    String body = "";
    String subject = "";
    String status = "";

    Object payload = eventContext.getMessage().getPayload();

    HashMap detailsEmail = (HashMap)payload;
    subject = (String)detailsEmail.get("subject");
    body = (String)detailsEmail.get("message");

    Properties props = new Properties();
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", this.enableTLS);
    props.put("mail.smtp.host", this.host);
    props.put("mail.smtp.port", this.port);

    System.out.println("USERNAME : " + username + " PORT : " + this.port + "TLS : " + props.getProperty("mail.smtp.starttls.enable"));
    Session session = Session.getInstance(props, 
      new Authenticator() {
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);
      }

    });
    try
    {
      Message message = new MimeMessage(session);
      message.setContent(body, "text/html; charset=utf-8");
      message.setFrom(new InternetAddress(username));
      message.setRecipients(Message.RecipientType.TO, 
        InternetAddress.parse((String)detailsEmail.get("email")));
      message.setSubject(subject);

      Transport.send(message);
      status = "success";
      System.out.println("Done");
    }
    catch (Exception e)
    {
      status = "failed";
      System.out.println(e);
    }

    return status;
  }
}